# README #

Bind Alias Detector Plugin

### Many cvars ###
The cfg will be auto-generated in `cfg/sourcemod/badplugin.cfg`.

The ban levels are:  
Very Low: 1  
Low: 2  
Medium: 3  
High: 4  

The log file for detections is located in "csgo/addons/sourcemod/logs/badplugins_log.txt"

// Custom commands to run when a player reaches minimum detection level.  
// Note that this will run even with the autoban cvars set to 0  
// {steamid} will get replaced with the banned player's steamid.  
// Example usage: "sm_bad_custom_bancmd "sm_resetplayerjumpstats {steamid}; sm_ban #{steamid} 10080 Null and -forward binds are not allowed on this server.""  
// -  
// Default: ""  
sm_bad_custom_bancmd ""  

// Whether to autoban people who get detected for using a -forward bind.  
// -  
// Default: "1"  
// Minimum: "0.000000"  
// Maximum: "1.000000"  
sm_bad_fwd_autoban "1"  

// -Forward detection levels for logging. 4 numbers, comma separated  
// All numbers have to be bigger than 0 and be in ascending order  
// -  
// Default: "10, 15, 20, 25"  
sm_bad_fwd_detectionlevels "10, 15, 20, 25"  

// Whether to log -forward bind detections or not.  
// -  
// Default: "1"  
// Minimum: "0.000000"  
// Maximum: "1.000000"  
sm_bad_fwd_logging "1"  

// Minimum detection level to autoban people.  
// -  
// Default: "4"  
// Minimum: "1.000000"  
// Maximum: "4.000000"  
sm_bad_fwd_min_banlevel "4"  

// Minimum detection level to log people in chat.  
// -  
// Default: "1"  
// Minimum: "1.000000"  
// Maximum: "4.000000"  
sm_bad_fwd_min_chatloglevel "1"  

// Minimum detection level to log people.  
// -  
// Default: "1"  
// Minimum: "1.000000"  
// Maximum: "4.000000"  
sm_bad_fwd_min_loglevel "1"  

// Whether to autoban people who get detected for using the null movement script.  
// -  
// Default: "1"  
// Minimum: "0.000000"  
// Maximum: "1.000000"  
sm_bad_null_autoban "1"  

// Null detection levels for logging. 4 numbers separated by commas  
// -  
// Default: "32, 64, 128, 256"  
sm_bad_null_detectionlevels "32, 64, 128, 256"  

// Whether to log null script detections or not.  
// -  
// Default: "1"  
// Minimum: "0.000000"  
// Maximum: "1.000000"  
sm_bad_null_logging "1"  

// Whether to autoban people who get detected for using the null movement script.  
// -  
// Default: "4"  
// Minimum: "1.000000"  
// Maximum: "4.000000"  
sm_bad_null_min_banlevel "4"  

// Minimum detection level to log to admins in chat.  
// -  
// Default: "1"  
// Minimum: "1.000000"  
// Maximum: "4.000000"  
sm_bad_null_min_chatloglevel "1"  

// Minimum detection level to log people.  
// -  
// Default: "1"  
// Minimum: "1.000000"  
// Maximum: "4.000000"  
sm_bad_null_min_loglevel "1"  


**v1.02 Changelog**

19.05.2020

Additions:

- 4 new cvars:  
 `sm_bad_fwd_min_loglevel` - Minimum detection level to log -forward detections.  
 `sm_bad_fwd_min_chatloglevel` - Minimum detection level to log -forward detections to admins in chat.  
 `sm_bad_null_min_loglevel` - Minimum detection level to log null detections.  
 `sm_bad_null_min_chatloglevel` - Minimum detection level to log null detections to admins in chat.  

Fixes and changes:

- Removed unnecessary integer from null detection logs.